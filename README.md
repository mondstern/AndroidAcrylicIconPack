[![Sanity checks status](https://ci.codeberg.org/api/badges/mondstern/AndroidAcrylicIconPack/status.svg)](https://ci.codeberg.org/mondstern/AndroidAcrylicIconPack/branches/main)

# Mondstern Acrylic Icons

A very simple icon pack of Mondstern's Acrylic icons.

Feel free to use this app code to make your own icon packs.

App is by Sylvia van Os under Creative Commons Zero 1.0 (CC0 1.0)  
Icons are by Mondstern under Creative Commons Attribution-ShareAlike 4.0 (CC BY-SA 4.0)

## Screenshots

[<img src="https://codeberg.org/mondstern/AndroidAcrylicIconPack/raw/branch/main/fastlane/metadata/android/en-US/images/phoneScreenshots/screenshot-01.png" width=250>](https://codeberg.org/mondstern/AndroidAcrylicIconPack/raw/branch/main/fastlane/metadata/android/en-US/images/phoneScreenshots/screenshot-01.png)
[<img src="https://codeberg.org/mondstern/AndroidAcrylicIconPack/raw/branch/main/fastlane/metadata/android/en-US/images/phoneScreenshots/screenshot-02.png" width=250>](https://codeberg.org/mondstern/AndroidAcrylicIconPack/raw/branch/main/fastlane/metadata/android/en-US/images/phoneScreenshots/screenshot-02.png)
[<img src="https://codeberg.org/mondstern/AndroidAcrylicIconPack/raw/branch/main/fastlane/metadata/android/en-US/images/phoneScreenshots/screenshot-03.png" width=250>](https://codeberg.org/mondstern/AndroidAcrylicIconPack/raw/branch/main/fastlane/metadata/android/en-US/images/phoneScreenshots/screenshot-03.png)
[<img src="https://codeberg.org/mondstern/AndroidAcrylicIconPack/raw/branch/main/fastlane/metadata/android/en-US/images/phoneScreenshots/screenshot-04.png" width=250>](https://codeberg.org/mondstern/AndroidAcrylicIconPack/raw/branch/main/fastlane/metadata/android/en-US/images/phoneScreenshots/screenshot-04.png)
