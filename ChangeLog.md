## Version 15, not online

packagename changed: element, flashy, innertune, news, partygames, pcapdroid piwigo, redreader, trail sense, unstoppable, wifi analyzer, wifi info

deleted: koyu. space

Anonaddy - neuer name: sddy.io, neues icon
Arch packages
Aurora Droid
Ariane
BLW Bitcoin lightning wallet
(axet) book reader
Barinsta
blabber.im
Clover
Cryptolitycs
CyberWOW
Derdiedas
Dateimanager (Simple Mobile Tools)
Elementary
EnRecipes
Eventyay Attendee
Eventyay Organizer
Flym
ForRunners
Fyreplace
Galerie (SMT)
K-9 Mail
Kontakte
LibreTrivia
Light Android Launcher
Manyverse
miniVector
Monochromatic
Movim
NiceFeed
Notepad errormeldung
NoFasel
OpenLauncher
OCReader
Piwigo
p≡p
Rechner (smt)
Shade Launcher
Simpletask Cloudless
SkyTube Extra
Scheduler
Sentien Launcher
SkyDroid
SMS Messenger
Seshat
Timetable
Tinc App
Tizeno
Torrent Client
Trezor Manager
twitlatte
Telefon (smt)
TTubeAcad
TubeLab
Twidere
tFileTransfer
WeeklyBudget
Wetter (beowulf)
Zim Launcher

new: ameixa, exodus, fdroid basic, skytube legacy

## Version 14

with: Woodpecker CI
new: Activity Manager, AgoraDesk, Always On Clock, Arity, AutoNotify, Auxio, blood pressure app, BOOM Switch, Cofi, 
CommandClick, Daten-Monitor, DAVx⁵, Doable, Dooz, Drinkable, Drum On!, EBT Compass, Energize, ente, Fake Traveler, 
FastLyrics, Fertility Test Analyzer, Focus Launcher, FTPClient, Godot Editor 3 + 4, gptAssist, Habo, Hacki, HalfDot, 
Improvement Roll, InnerTune, jtx Board, JustNotes, JustWeather, kboard Settings, KernelSU, Khanos, mail, Mattermost Beta, Menza, Meshtastic, Miniflutt, mpv, mucke, Nextcloud News, neutriNote CE, Next Player, NoFasel, Olauncher, Olauncher Clutter Free, OpenStop, OpenVK, OSS Wetter, ParkenUlm, Phase 10 Counter, PipePipe, PlainApp, Quote Unquote, RxDroid, Screen Time, Sensor Server, SimpleX, SongTube, Nextcloud Tables, Task Managerm, Tempus Romanum, Termux:GUI, translator, tSotaLog, Tuner, Twire, Ultrasonic, URLCheck, V2compose, Virtual Hosts,<>Voice, Wake on Lan, Water Me, Wikipedia and Yivi

changed: activity name of Opentracks, Nextcloud Bookmarks

deleted: Full Colemak, riot

## Version 13

fixes: Ruslin -> ruslin

## Version 12

with: Codeberg CI

changed: package name of Pixeldroid

new: Diaguard, Openreads, mute reminder, ruslin, falling blocks, open canteen, drip, parlera, graphhopper, seshat, wger, weather, heartest and anotherpass

## Version 11

new: Codeberg CI

changed: package name of derdiedas

## Version 10

fixes usageDirect -> usagedirect

## Version 9

new: 40 icons<br>

Arcticons Dark, Arcticons Light, Aurora Droid, AVNC, droidVNC-NG,  Delist, Exif Thumbnail Adder,
Feeel, Fdroid nearby, Funkwhale, Friendica, GymRoutines, Giant Stopwatch,  Guerrilla Mail, 
Home Assistant,  Just Video Player, monocles browser, Museum of a broken API, nabla, 
oRing - Reminder, opmt, openrec, peercoin, pushie, Remote Numpad, Silectric, SelfPrivacy,
ShoppingList, Subtracks, Timer +X, Todont, Tickmate, tFileTransfer, Trackbook, usageDirect,
URLSanitizer, Voltage Drop Calculator, WalletCount, WeeklyBudget amd Workouts'N'Rest

## Version 8

fixes aTox -> atox

## Version 7

new: 79 icons<br>

eventyayorganizerapp, 2playerbattle, acode, acrylicicons, antimine, ariane, asteroidos, aTox, badgemagic, barinsta, bitwarden, blabberim, blockpuzzle, bolton, dagger, deedum, deltachat, discreetlauncher, elementary, eventyayattendeeapp, fdroidbuildstatus, fdroidclassic, flashy, geonotes, grocyandroid, httpshortcuts, imagepipe, imgurviewer, instalate, jiofibatterynotifier, justanotherworkouttimer, k9mail, kfzkennzeichen, liberovocab, medilog, memory, metro, midictrl, nextcloudbookmarks, notally, noteless, ocreader, okcagent, opencamera, osmdashboard, osmdashboardoffline, pep, periodical, photok, pixeldroid, posidonlauncher, powertunnel, proexpense, rechnen, retro, runner, schildichat, sentienlauncher, sharetoinputstick, sharik, simplecalculator, simplecontactspro, simpledialer, simplefilemanagerpro, simplegallerypro, simplesmsmessenger, skydroid, teddyforreddit, tizeno, todoagenda, trailsense, transistor, tubeacad, tubelab, tvrandshow, twidere, voipmssms, watomatic and wlanscanner<br>

exchanged: nextcloudcookbook of micmun (new logo)<br>

what is new from now on<br>

all icons are only listed one below the other, i.e. from version 1.0 to 6.0 everything was from A to Z. from version 7.0 onwards, all new icons are listed below and not everything from A to Z any more<br>

## Version 6

fixes 53 icons with borders<br>

archpackages, binaryeye, bookreader, briar, clover, dailydozen, dandelior, derdiedas, diab, dmsexplorer, emeraldialer, eprssreader, etesync, fdroid, flym, huewidgets, kisslauncher, kodi, kore, krautschluessel, lightandroidlauncher, markor, minivector, monerujo, monochromatic, movim, newpipe, notepad, oeffi, opencontacts, opendnsupdater, openhab, openlauncher, openscale, riot, rxdroid, sdbviewer, skytube, streetcomplete, suntimes, superfreeze, syncopoli, termux, timelimit, timetable, transportr, trigger, tusky, ulogger, unstoppable, vanillamusic, vocabletrainer and zapp<br>

## Version 5

fixes display of incorrectly displayed icon:<br>

vespucci<br> 

## Version 4

includes the following icons:<br>

bettercounter, exoairplayer, nextcloud, nextcloudcookbook, nextcloudcookbookflutter,<br> nextcloudyaga, nexttracks, nicefeed, nlweer, notepad, notificationcron, notificationlog, <br>
ocr, odeon, oeffi, opencontacts, opendnsupdater, openhab, openlauncher, openmultimaps, <br>
openmensa, openscale, opentodolist, opentracks, osmand, p2play, pcapdroid, piwigo, <br>
postwriter, potatoproject notes, pro expense, pslab, pulsemusic, railwaystationphotos,<br> raisetoanswer, redmoon, rxdroid, scrambled exif, screenshot tile, sdbviewer, <br>
shadelauncher, shadowsocks foss, shopwithmom, simpletask, skytube, snsmatrix, <br>
streetcomplete, suntimes, superfreeze, syncopoli, taupunkt, teddyforreddit, termbin, <br>
termux, texttorch, timelimit, timetable, tincapp, tinykeepass, torrentclient, <br>
trackercontrol, transportr, trezormanager, trigger, tschernobyl, tusky, <br>
tiny weather forecast germany, twitlatte, ulogger, unit converter ultimate, <br>
unstoppable, vanillamusic, vector pinball, vertretungsplan, vespucci, vocabletrainer,<br> volumecontrol, volumenotification, wakelock, wanidoku, wifianalyzer, wifi info,<br> 
x11basic, xmppadmin, zapp, zimlauncher<br>

## Version 3

includes the following icons:<br>

fairemail, fdroid, fedilab, feeder, fissh, fitotrack, florisboard, fly, forecasttie, <br>
forrunners, fullcolemak, fyreplace, gadgetbridge, geonotes, gitnex, greenbitcoinwallet, <br>
homeapp, homebot, hourlyreminder, huewidgets, hypezihg, joplin, kisslauncher, kodi, <br>
kontalk, kore, koyuspace, krautschlüssel, librehome, libreipsum, libretrivia,<br> lightandroidlauncher, log28, loophabbittracker, lrceditor, manyverse, markor,<br> 
masstransfer, materialfiles, metagersuche, mifareclassictool, minivector, mobilizon,<br> 
monerujo, moneytracker, monochromatic, morse, movim, musicalnotes, newpipe<br>

## Version 2

fixes display of incorrectly displayed icons:<br>

androidversion, anothernotes, appsmonitor, bitcoinpluslightningwallet, calculatorplusplus<br> 

## Version 1

the first version includes the following icons:<br>

1list, 9p, aegis, aicamera, androidversion, anonaddy, anothernotesv, appmonitor, archpakages<br> audioserve, babydots, balancetheball, baresip, binaryeye, bitcoinpluslightningwallet,<br> 
blockpuzzle, bookreader, briar, calculatorplusplus, callrecorder, campfire, catima<br> 
chubbyclick, clover, collaboraoffice, contactdiary, cryptolitycs, currencies, cyberwow<br> dailydozen, dandelior, dapnet, derdiedas, deufeitage, diab, diary, dmsexplorer, easynotesbr<br>
element, emeraldialer, enrecipes, eprssreader, escapepod, etar, etesync, subz<br>